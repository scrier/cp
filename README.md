# Exercise 1 - Image recognition

 ![](tcp-girls_chatting.jpeg)

## Features

 1. Mug
 2. Persons
 3. Computer
 4. Shoes
 5. Watch

## Prohibited Content

Persons are dressed, usually you train on breast/nipples and other similar parts of the body.Difficulty here is the 
black and white as most prohibited content is trained on colored content. Using filters such as black and white 
(most simple) to inverting colors (much harder) should be trained on a network detecting filters instead. And 
viewed if flagged, if we f.e. are talking about a moderation system.

# Exercise 2 - Weather forecast and alerts.


As this is a web app and I don't work with webapp I will take discussion about it. I could easily google something
and put it in as code, but as it's not my code I feel it will be misleading.

As the api is lat/long based, you need to first get location to lat/long using theur geocoding API.

https://openweathermap.org/api/geocoding-api

Using this information you can fetch the current weather given any location in the world.

https://openweathermap.org/current

Architectural principle is to provide this through a API where you create business logic and simplified
API to the frontend service where you can lookup and provide response for a given location directly.

## Part 2, push notifications.

As for Global Weather Alerts they provide a push service (https://openweathermap.org/api/push-weather-alerts) 
and this is the best way to get push updates where you expose an endpoint to receive the data from OpenWeather.
It's a bit crude compared to using websocket, but it is what you can use to get push notifications on weather 
changes on a global scale. Once again putting this though a backend where you provide a websocket until they
provide this would be a nice implementation.

## Improvements:

 * As there is a subscription model I would rate limit on the backend side, and cache information to be within 
   the payed for subscription.

# Exercise 3 - System architecture design

[Exercice 3 PDF scribble](exercice%203.pdf)

 * Pricing
   First Iteration
     * Cost: Cluster + Pods ~65$ a month
     * Cost: Database ~55$ a month
   Second Iteratiom
     * Cost: Cluster + Pods ~87$ a month
     * Cost: Databse ~55$ a month
 * Container Orchestration
   * Host shares the OS with other applications. Containers is their own OS and contained within a docker image.
   * Container Orchestration is what Kubernetes or Marathon Mesos does. Distribute containers over nodes and manage them.
 * Cloud Migration
   * Benefits is reduced maintenance for your own infrastructure and getting the experience from people working with it on scale for years. There are also alot easier integrations between tooling as that is things resolved once and you can benefit from. Maintaining your own big data database (clickhouse f.e.) is alot of work compared to using out of the box from cloud vendors.
   * Biggest risk is issues concerning data and where it is stored. GDPR is a major issue and the major cloud vendors are under US law and can at any given time provide the data to US government. 
